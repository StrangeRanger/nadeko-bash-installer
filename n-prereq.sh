#!/bin/bash -e
# Install dotnet
root=$(pwd)
echo ""

function INSTALL_YTDLP {
    wget https://github.com/yt-dlp/yt-dlp/releases/latest/download/yt-dlp -O ~/.local/bin/yt-dlp
    chmod a+rx ~/.local/bin/yt-dlp
}

wget -q -N https://gitlab.com/Kwoth/nadeko-bash-installer/-/raw/v5/detectOS.sh
declare DETECTED=($(bash detectOS.sh))

if [[ ${DETECTED[0]} = "" ]]; then exit 1; fi

OS="${DETECTED[0]}"
VER="${DETECTED[1]}"
ARCH="${DETECTED[2]}"
SVER="${DETECTED[3]}"

echo "This installer will download all of the required packages for NadekoBot. It will use about 350MB of space. This might take awhile to download if you do not have a good internet connection.\n"
echo -e "Would you like to continue? \nYour OS: $OS \nOS Version: $VER \nArchitecture: $ARCH"

while true; do
    read -p "[y/n]: " yn
    case $yn in
        [Yy]* ) clear; echo Running NadekoBot Auto-Installer; sleep 2; break;;
        [Nn]* ) echo Quitting...; rm n-prereq.sh && exit;;
        * ) echo "Couldn't get that please type [y] for Yes or [n] for No.";;
    esac
done

echo ""

if [ "$OS" = "Ubuntu" ]; then
    if [ "$VER" = "23.10" ]; then
        echo -e "*Ubuntu 23.10 will reach End Of Life (EOL) on July 01, 2024. For more information, see the official Ubuntu EOL page. "
    fi
    echo "Installing dotnet"
    wget "https://packages.microsoft.com/config/ubuntu/$VER/packages-microsoft-prod.deb" -O packages-microsoft-prod.deb
    sudo dpkg -i packages-microsoft-prod.deb
    rm packages-microsoft-prod.deb

    sudo apt-get update;
    sudo apt-get install -y apt-transport-https && sudo apt-get update;
    sudo apt-get install -y dotnet-sdk-8.0;

    echo "Installing Git and Tmux..."
    sudo apt-get install git tmux -y

    echo "Installing music prerequisites..."
    sudo apt-get install libopus0 opus-tools libopus-dev libsodium-dev python ffmpeg -y
    echo ""
    INSTALL_YTDLP

elif [ "$OS" = "Debian" ]; then
    if [[ "$SVER" == "9" ]]; then
        echo "Support for Debian 9 has reached End of Life (EOL) as of August 9, 2022"
        echo "Please upgrade to Debian 10 or newer"
        rm n-prereq.sh
        exit 1
    fi

    echo "Installing dotnet..."
    wget https://packages.microsoft.com/config/debian/"$SVER"/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
    sudo dpkg -i packages-microsoft-prod.deb
    rm packages-microsoft-prod.deb

    sudo apt-get update; \
      sudo apt-get install -y apt-transport-https && \
      sudo apt-get update && \
      sudo apt-get install -y dotnet-sdk-8.0

    echo "Installing Git and Tmux..."
    sudo apt-get install git tmux -y

    echo "Installing music prerequisites..."
    sudo apt-get install libopus0 libopus-dev libsodium-dev ffmpeg -y
    echo ""
    INSTALL_YTDLP

elif [ "$OS" = "Fedora" ]; then
    sudo dnf -y install dotnet-sdk-8.0
    sudo dnf -y install git tmux

    sudo dnf -y install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
    sudo dnf -y install ffmpeg
    sudo dnf -y install opus-tools opus libsodium
    INSTALL_YTDLP

elif [ "$OS" = "openSUSE Leap" ] || [ "$OS" = "openSUSE Tumbleweed" ]; then
    echo -e "Installing dotnet..."
    sudo zypper install -y libicu wget
    sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
    wget https://packages.microsoft.com/config/opensuse/15/prod.repo
    sudo mv prod.repo /etc/zypp/repos.d/microsoft-prod.repo
    sudo chown root:root /etc/zypp/repos.d/microsoft-prod.repo
    sudo zypper install -y dotnet-sdk-8.0

    echo -e "\nInstalling git, tmux..."
    sudo zypper install -y git tmux

    echo -e "\nInstalling music prerequisites..."
    sudo zypper install -y ffmpeg libopus0 yt-dlp

elif [ "$OS" = "LinuxMint" ]; then
    echo "Installing Git and Tmux..."
    sudo apt-get update;
    sudo apt-get install -y git tmux

    echo "Installing dotnet..."
    if [ "$SVER" = "19" ] || [ "$SVER" = "20" ]; then
        wget https://packages.microsoft.com/config/ubuntu/20.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
    elif [ "$SVER" = "21" ]; then
        wget https://packages.microsoft.com/config/ubuntu/22.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
    fi

    sudo dpkg -i packages-microsoft-prod.deb
    rm packages-microsoft-prod.deb
    sudo apt-get update && \
      sudo apt-get install -y dotnet-sdk-8.0

    sudo apt-get install -y apt-transport-https && \
      sudo apt-get update;

    echo "Installing music prerequisites..."
    sudo apt-get install -y libopus0 opus-tools libopus-dev libsodium-dev ffmpeg
    INSTALL_YTDLP

elif [ "$OS" = "AlmaLinux" ] || [ "$OS" = "Rocky Linux" ]; then
    echo "Installing dotnet..."
    sudo dnf install -y dotnet-sdk-8.0

    echo "Installing Git and Tmux..."
    sudo dnf install -y wget git opus tmux python3.11

    echo "Installing music prerequisites..."

    if [ "$SVER" = "8" ]; then
        sudo dnf -y install https://download.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
        sudo yum install yum-utils -y
        sudo yum-config-manager --enable powertools
    fi

    sudo dnf install -y --nogpgcheck https://mirrors.rpmfusion.org/free/el/rpmfusion-free-release-$(rpm -E %rhel).noarch.rpm
    sudo dnf install -y ffmpeg
    INSTALL_YTDLP

elif [ "$OS" = "Darwin" ]; then
    brew update
    brew install wget git ffmpeg openssl opus opus-tools opusfile libffi libsodium tmux python yt-dlp

    brew install mono-libgdiplus
fi

echo
echo "NadekoBot Prerequisites Installation completed..."
read -n 1 -s -p "Press any key to continue..."
sleep 2

cd "$root"
rm "$root/n-prereq.sh"
exit 0
